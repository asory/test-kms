let sdk;

function createSpanishKms() {
  const InbentaKmSDK = document.querySelector(
    'script[src$="inbenta-km-sdk.js"]',
  );
  // Inbenta Public & Domain keys
  const InbAuth = {
    publicKey: ' BWtB3/ew4gSJ5KH5G8P5isGXaCwvCoymT9OFoT8hfrQ=',
    domainKey:
      'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJwcm9qZWN0Ijoic2t5X2ZhcV9lcyIsImRvbWFpbl9rZXlfaWQiOiJCWXBnRzFnVUNFMVV2VGJGUk8tZGhROjoifQ.LlXqR9-WeNsNnF3exk3TUu9EKU23cYYnrlMNW39GxbOAvj9FgrbSwfESuRQGTRk8nqRrFNLq07u8VskygVa5xw',
  };

  // Configuration instance
  const InbConfig = {
    environment: 'production', // Environments => "development" /  "production"
    maxQuestionCharacters: 50,
    lang: 'es',
    labels: {
    },
  };
 return InbentaKmSDK.createFromDomainKey(InbAuth.domainKey, InbAuth.publicKey, InbConfig);
}

function setKmsConfig() {
  const spanishHomeMarkets = [ 'argentina', 'chile', 'peru', 'otros'];
  const homeMarketByPath = document.location.pathname.split('/')[1];
  if (!spanishHomeMarkets.includes(homeMarketByPath)) return;
   sdk = createSpanishKms();
}

function createInbentaKmsSDK() {
  const title = document.getElementsByTagName('title')[0];
  const scriptInbentaKmsSDK = document.createElement('script');
  scriptInbentaKmsSDK.onload = () => {
    setKmsConfig();
  };
  scriptInbentaKmsSDK.src = 'https://sdk.inbenta.io/km/1/inbenta-km-sdk.js';
  scriptInbentaKmsSDK.integrity = 'sha384-5SAzy0v2YMX1NHcUSm3+DoU5NpVHAuop/bdHHWImdX43YPlr15S0z8cNCiEGu5LC';
  scriptInbentaKmsSDK.crossOrigin = 'crossorigin';
  scriptInbentaKmsSDK.setAttribute('defer', '');
  title.insertAdjacentElement('beforebegin', scriptInbentaKmsSDK);
}

function getKmsSDK() {
  return sdk;
}
document.addEventListener('DOMContentLoaded', () => {
  createInbentaKmsSDK();
});

exports{
  getKmsSDK,
}