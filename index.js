const express = require("express");
const app = express();
const path = require('path');

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname +"/template.html"));
});

app.listen(process.env.PORT || 9000, () => {
  console.log(`Example app listening at http://localhost:9000`);
});
